﻿namespace Game
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlGui = new System.Windows.Forms.Panel();
            this.lblPlayer = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnLeaderboard = new System.Windows.Forms.Button();
            this.btnOptions = new System.Windows.Forms.Button();
            this.btnLobby = new System.Windows.Forms.Button();
            this.btnStartMulti = new System.Windows.Forms.Button();
            this.btnStartSingle = new System.Windows.Forms.Button();
            this.pnlLogin = new System.Windows.Forms.Panel();
            this.btnNewAcc = new System.Windows.Forms.Button();
            this.lblFailedLogin = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lblPass = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblSubtitle = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.pnlControl = new System.Windows.Forms.Panel();
            this.pnlGui.SuspendLayout();
            this.pnlLogin.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlGui
            // 
            this.pnlGui.BackColor = System.Drawing.Color.RoyalBlue;
            this.pnlGui.Controls.Add(this.lblPlayer);
            this.pnlGui.Controls.Add(this.btnExit);
            this.pnlGui.Controls.Add(this.btnLeaderboard);
            this.pnlGui.Controls.Add(this.btnOptions);
            this.pnlGui.Controls.Add(this.btnLobby);
            this.pnlGui.Controls.Add(this.btnStartMulti);
            this.pnlGui.Controls.Add(this.btnStartSingle);
            this.pnlGui.Location = new System.Drawing.Point(12, 49);
            this.pnlGui.Name = "pnlGui";
            this.pnlGui.Size = new System.Drawing.Size(710, 438);
            this.pnlGui.TabIndex = 0;
            this.pnlGui.Visible = false;
            // 
            // lblPlayer
            // 
            this.lblPlayer.AutoSize = true;
            this.lblPlayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPlayer.ForeColor = System.Drawing.Color.Orange;
            this.lblPlayer.Location = new System.Drawing.Point(3, 7);
            this.lblPlayer.Name = "lblPlayer";
            this.lblPlayer.Size = new System.Drawing.Size(47, 15);
            this.lblPlayer.TabIndex = 13;
            this.lblPlayer.Text = "Player";
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExit.BackColor = System.Drawing.Color.SteelBlue;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnExit.ForeColor = System.Drawing.Color.Orange;
            this.btnExit.Location = new System.Drawing.Point(275, 372);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(142, 53);
            this.btnExit.TabIndex = 12;
            this.btnExit.Tag = "EX";
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.HandleNavigation);
            // 
            // btnLeaderboard
            // 
            this.btnLeaderboard.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnLeaderboard.BackColor = System.Drawing.Color.SteelBlue;
            this.btnLeaderboard.FlatAppearance.BorderSize = 0;
            this.btnLeaderboard.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLeaderboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnLeaderboard.ForeColor = System.Drawing.Color.Orange;
            this.btnLeaderboard.Location = new System.Drawing.Point(275, 231);
            this.btnLeaderboard.Name = "btnLeaderboard";
            this.btnLeaderboard.Size = new System.Drawing.Size(142, 53);
            this.btnLeaderboard.TabIndex = 11;
            this.btnLeaderboard.Tag = "LB";
            this.btnLeaderboard.Text = "Leaderboard";
            this.btnLeaderboard.UseVisualStyleBackColor = false;
            this.btnLeaderboard.Click += new System.EventHandler(this.HandleNavigation);
            // 
            // btnOptions
            // 
            this.btnOptions.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOptions.BackColor = System.Drawing.Color.SteelBlue;
            this.btnOptions.FlatAppearance.BorderSize = 0;
            this.btnOptions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnOptions.ForeColor = System.Drawing.Color.Orange;
            this.btnOptions.Location = new System.Drawing.Point(275, 304);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(142, 53);
            this.btnOptions.TabIndex = 10;
            this.btnOptions.Tag = "OP";
            this.btnOptions.Text = "Options";
            this.btnOptions.UseVisualStyleBackColor = false;
            this.btnOptions.Click += new System.EventHandler(this.HandleNavigation);
            // 
            // btnLobby
            // 
            this.btnLobby.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnLobby.BackColor = System.Drawing.Color.SteelBlue;
            this.btnLobby.FlatAppearance.BorderSize = 0;
            this.btnLobby.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLobby.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnLobby.ForeColor = System.Drawing.Color.Orange;
            this.btnLobby.Location = new System.Drawing.Point(275, 159);
            this.btnLobby.Name = "btnLobby";
            this.btnLobby.Size = new System.Drawing.Size(142, 53);
            this.btnLobby.TabIndex = 9;
            this.btnLobby.Tag = "LO";
            this.btnLobby.Text = "Lobby";
            this.btnLobby.UseVisualStyleBackColor = false;
            this.btnLobby.Click += new System.EventHandler(this.HandleNavigation);
            // 
            // btnStartMulti
            // 
            this.btnStartMulti.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStartMulti.BackColor = System.Drawing.Color.SteelBlue;
            this.btnStartMulti.FlatAppearance.BorderSize = 0;
            this.btnStartMulti.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnStartMulti.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnStartMulti.ForeColor = System.Drawing.Color.Orange;
            this.btnStartMulti.Location = new System.Drawing.Point(275, 86);
            this.btnStartMulti.Name = "btnStartMulti";
            this.btnStartMulti.Size = new System.Drawing.Size(142, 53);
            this.btnStartMulti.TabIndex = 8;
            this.btnStartMulti.Tag = "MP";
            this.btnStartMulti.Text = "Multi Player";
            this.btnStartMulti.UseVisualStyleBackColor = false;
            this.btnStartMulti.Click += new System.EventHandler(this.HandleNavigation);
            // 
            // btnStartSingle
            // 
            this.btnStartSingle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStartSingle.BackColor = System.Drawing.Color.SteelBlue;
            this.btnStartSingle.FlatAppearance.BorderSize = 0;
            this.btnStartSingle.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnStartSingle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnStartSingle.ForeColor = System.Drawing.Color.Orange;
            this.btnStartSingle.Location = new System.Drawing.Point(275, 18);
            this.btnStartSingle.Name = "btnStartSingle";
            this.btnStartSingle.Size = new System.Drawing.Size(142, 53);
            this.btnStartSingle.TabIndex = 7;
            this.btnStartSingle.Tag = "SP";
            this.btnStartSingle.Text = "Single Player";
            this.btnStartSingle.UseVisualStyleBackColor = false;
            this.btnStartSingle.Click += new System.EventHandler(this.HandleNavigation);
            // 
            // pnlLogin
            // 
            this.pnlLogin.BackColor = System.Drawing.Color.RoyalBlue;
            this.pnlLogin.Controls.Add(this.btnNewAcc);
            this.pnlLogin.Controls.Add(this.lblFailedLogin);
            this.pnlLogin.Controls.Add(this.btnLogin);
            this.pnlLogin.Controls.Add(this.lblPass);
            this.pnlLogin.Controls.Add(this.lblUser);
            this.pnlLogin.Controls.Add(this.lblSubtitle);
            this.pnlLogin.Controls.Add(this.lblTitle);
            this.pnlLogin.Controls.Add(this.txtPass);
            this.pnlLogin.Controls.Add(this.txtUser);
            this.pnlLogin.Location = new System.Drawing.Point(105, 12);
            this.pnlLogin.Name = "pnlLogin";
            this.pnlLogin.Size = new System.Drawing.Size(711, 487);
            this.pnlLogin.TabIndex = 0;
            // 
            // btnNewAcc
            // 
            this.btnNewAcc.BackColor = System.Drawing.Color.SteelBlue;
            this.btnNewAcc.FlatAppearance.BorderSize = 0;
            this.btnNewAcc.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNewAcc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnNewAcc.ForeColor = System.Drawing.Color.Orange;
            this.btnNewAcc.Location = new System.Drawing.Point(343, 326);
            this.btnNewAcc.Name = "btnNewAcc";
            this.btnNewAcc.Size = new System.Drawing.Size(142, 53);
            this.btnNewAcc.TabIndex = 8;
            this.btnNewAcc.Text = "New Account";
            this.btnNewAcc.UseVisualStyleBackColor = false;
            this.btnNewAcc.Click += new System.EventHandler(this.btnNewAcc_Click);
            // 
            // lblFailedLogin
            // 
            this.lblFailedLogin.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblFailedLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblFailedLogin.ForeColor = System.Drawing.Color.Red;
            this.lblFailedLogin.Location = new System.Drawing.Point(0, 440);
            this.lblFailedLogin.Name = "lblFailedLogin";
            this.lblFailedLogin.Size = new System.Drawing.Size(711, 47);
            this.lblFailedLogin.TabIndex = 7;
            this.lblFailedLogin.Text = "Password or user name are incorrect!";
            this.lblFailedLogin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.SteelBlue;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnLogin.ForeColor = System.Drawing.Color.Orange;
            this.btnLogin.Location = new System.Drawing.Point(343, 242);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(142, 53);
            this.btnLogin.TabIndex = 6;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.authorise_Click);
            // 
            // lblPass
            // 
            this.lblPass.AutoSize = true;
            this.lblPass.BackColor = System.Drawing.Color.Transparent;
            this.lblPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPass.ForeColor = System.Drawing.Color.Orange;
            this.lblPass.Location = new System.Drawing.Point(173, 195);
            this.lblPass.Name = "lblPass";
            this.lblPass.Size = new System.Drawing.Size(91, 20);
            this.lblPass.TabIndex = 5;
            this.lblPass.Text = "Password:";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.BackColor = System.Drawing.Color.Transparent;
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblUser.ForeColor = System.Drawing.Color.Orange;
            this.lblUser.Location = new System.Drawing.Point(154, 137);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(131, 20);
            this.lblUser.TabIndex = 4;
            this.lblUser.Text = "Account Name:";
            // 
            // lblSubtitle
            // 
            this.lblSubtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtitle.ForeColor = System.Drawing.Color.Orange;
            this.lblSubtitle.Location = new System.Drawing.Point(4, 69);
            this.lblSubtitle.Name = "lblSubtitle";
            this.lblSubtitle.Size = new System.Drawing.Size(796, 23);
            this.lblSubtitle.TabIndex = 3;
            this.lblSubtitle.Text = "subTitle";
            this.lblSubtitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Orange;
            this.lblTitle.Location = new System.Drawing.Point(307, 20);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(222, 46);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "BattleShipz";
            // 
            // txtPass
            // 
            this.txtPass.BackColor = System.Drawing.Color.SteelBlue;
            this.txtPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPass.ForeColor = System.Drawing.Color.Orange;
            this.txtPass.Location = new System.Drawing.Point(291, 189);
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(238, 29);
            this.txtPass.TabIndex = 1;
            // 
            // txtUser
            // 
            this.txtUser.BackColor = System.Drawing.Color.SteelBlue;
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUser.ForeColor = System.Drawing.Color.Orange;
            this.txtUser.Location = new System.Drawing.Point(291, 131);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(238, 29);
            this.txtUser.TabIndex = 0;
            // 
            // pnlControl
            // 
            this.pnlControl.BackColor = System.Drawing.Color.RoyalBlue;
            this.pnlControl.Location = new System.Drawing.Point(2, -1);
            this.pnlControl.Name = "pnlControl";
            this.pnlControl.Size = new System.Drawing.Size(97, 44);
            this.pnlControl.TabIndex = 1;
            this.pnlControl.Visible = false;
            this.pnlControl.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.pnlControl_ControlRemoved);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(816, 499);
            this.Controls.Add(this.pnlControl);
            this.Controls.Add(this.pnlGui);
            this.Controls.Add(this.pnlLogin);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game";
            this.pnlGui.ResumeLayout(false);
            this.pnlGui.PerformLayout();
            this.pnlLogin.ResumeLayout(false);
            this.pnlLogin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlGui;
        private System.Windows.Forms.Panel pnlLogin;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Label lblPass;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblSubtitle;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label lblFailedLogin;
        private System.Windows.Forms.Button btnNewAcc;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnLeaderboard;
        private System.Windows.Forms.Button btnOptions;
        private System.Windows.Forms.Button btnLobby;
        private System.Windows.Forms.Button btnStartMulti;
        private System.Windows.Forms.Button btnStartSingle;
        private System.Windows.Forms.Panel pnlControl;
        private System.Windows.Forms.Label lblPlayer;
    }
}