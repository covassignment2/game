﻿using System;

namespace Game.Model
{
    [Obsolete("Obsolete. Use DTO class equivalent instead")]
    public class LeaderboardData
    {
        public int Id;

        public int PlayerId;

        public int Rank;

        public string PlayerName;

        public float Score;
    }
}
