﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
    [Obsolete("Obsolete. Use DTO class equivalent instead")]
    public class Player
    {
        public Player() { }
        public int Id;
        public string Name;
        public string Password; //byte[]
        public int LeaderboardPosition;

        public void SereialiseToJson()
        {

        }

        public void DeserialiseFromJson()
        {

        }
    }
 
 }
