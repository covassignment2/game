﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
    public class GameState
    {
        public GameState(int noShips, bool hasAi)
        {
            MapPlayerOne = new PlayerMap(10, 10, noShips);
            MapPlayerTwo = new PlayerMap(10, 10, noShips);
            
            PlayerOneData = new PlayerGameData();

            if (!hasAi)
            {
                PlayerTwoData = new PlayerGameData();
                return;
            }
            HasAI = hasAi;

            NPC = new NonPlayerController();
            MapPlayerTwo = NPC.SelectLocations(MapPlayerTwo);

        }

        public bool HasAI;

        public PlayerNumber CurrentPlayerTurn;

        public PlayerMap MapPlayerOne;

        public PlayerMap MapPlayerTwo;

        public PlayerGameData PlayerOneData;

        public PlayerGameData PlayerTwoData;

        public NonPlayerController NPC;

        public bool IsGameOver;

        public PlayerNumber Winner;

      

    }
}
