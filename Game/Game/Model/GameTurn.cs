﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
    public class GameTurn
    {
        public GameTurn() { }

        public PlayerNumber CurrentPlayer;

        public int AttackPositionX;

        public int AttackPositionY;
    }
}
