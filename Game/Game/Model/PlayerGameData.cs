﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
    public class PlayerGameData
    {
        public PlayerGameData()
        {

        }

        public string PlayerName;

        public PlayerNumber PlayerNumber;

        public PlayerState CurrentState = PlayerState.Selecting;

        public PlayerType Type = PlayerType.Human;

    }
}
