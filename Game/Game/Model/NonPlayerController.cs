﻿using Game.Model;

namespace Game.Model
{
    public class NonPlayerController : PlayerGameData
    {
        public NonPlayerController()
        {
            this.PlayerName = "Bob the AI";
            this.Type = PlayerType.ArtificialIntelligence;
        }

        public PlayerMap SelectLocations(PlayerMap aiMap)
        {
            bool canPlace = aiMap.HasShipsToPlace();

            while (canPlace)
            {
                int posX = GetRandomCoordinate(aiMap.GetWidth());
                int posY = GetRandomCoordinate(aiMap.GetHeight());

                if(aiMap.EnquirePoint(posX, posY) == MapComponent.Empty)
                {
                    aiMap.SelectPoint(posX, posY);
                }

                canPlace = aiMap.HasShipsToPlace();
            }

            return aiMap;

        } 

        private int GetRandomCoordinate(int max)
        {
            System.Random random = new System.Random();

            int randomPos = random.Next(0, max);
            return randomPos;
        }


    }
}
