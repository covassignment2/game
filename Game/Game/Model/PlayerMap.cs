﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
    public class PlayerMap
    {
        private int _width;
        private int _height;
        private int _noShips;
        private int _noShipsLeft;

        public MapComponent[][] MapData;

        public PlayerMap(int width, int height, int noShips)
        {
            _height = height;
            _width = width;

            MapData = new MapComponent[_width][];

            InitialiaseArray(MapData);

            _noShips = noShips;
            _noShipsLeft = noShips;
        }

        public void SelectPoint(int positionX, int positionY)
        {
            AlterPoint(positionX, positionY, MapComponent.Ship);
            _noShips--;
        }

        public void DeselectPoint(int positionX, int positionY)
        {
            AlterPoint(positionX, positionY, MapComponent.Empty);
            _noShips++;
        }

        public bool AttackPoint(int positionX, int positionY)
        {
            MapComponent newPoint = MapComponent.Miss;

            if (EnquirePoint(positionX, positionY) == MapComponent.Ship)
            {
                newPoint = MapComponent.Hit;
                _noShipsLeft--;
            }

            AlterPoint(positionX, positionY, newPoint);

            return newPoint == MapComponent.Hit;
        }

        public MapComponent EnquirePoint(int positionX, int positionY)
        {
            return MapData[positionX][positionY];
        }

        public bool HasShipsToPlace()
        {
            return _noShips > 0;
        }

        public bool HasShipsAlive()
        {
            return _noShipsLeft == 0;
        }

        public int ShipsToPlace()
        {
            return _noShips;
        }

        public int ShipsAlive()
        {
            return _noShipsLeft;
        }

        public int GetHeight()
        {
            return _height;
        }

        public int GetWidth()
        {
            return _width;
        }

        #region Private Functions
        
        private void InitialiaseArray(MapComponent[][] comp)
        {
            for (int i = 0; i < _width; i++)
            {
                comp[i] = new MapComponent[_height];
            }
        }

        private void AlterPoint(int positionX, int positionY, MapComponent newComopent)
        {
            MapData[positionX][positionY] = newComopent;
        }

        #endregion


    }
}
