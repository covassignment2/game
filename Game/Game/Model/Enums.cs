﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model
{
    public enum MapComponent
    {
        Empty = 0,
        Ship = 1,
        Hit = 2,
        Miss = 3
    }

    public enum PlayerNumber
    {
        PlayerOne = 0,
        PlayerTwo = 1
    }

    public enum PlayerState
    {
        Selecting = 0,
        Waiting = 1,
        Attacking = 2,
        Defending = 3,
        Won = 4,
        Lost = 5
    }

    public enum PlayerType
    {
        ArtificialIntelligence = 0,
        Human = 1
    }
}
