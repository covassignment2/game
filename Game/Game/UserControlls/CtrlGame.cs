﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Game.Controller;
using Game.Model;

namespace Game.UserControlls
{
    public partial class CtrlGame : CtrlBase
    {
        private const int _mapSize = 10;
        private PlayerNumber _playerNo = PlayerNumber.PlayerOne;

        private PlayerState _currentState = PlayerState.Selecting;
        
        private MechanicsController _mechanicsController = new MechanicsController(true);

        private DTO.dtoPlayer _player;

        private bool _gameOver = false;

        #region Constructor

        public CtrlGame()
        {
            InitializeComponent();

            InitialiseColumns();

            InitialiseRows();

            btnAction.Text = "Finish Selecting";

            dgEnemy.Enabled = false;

        }

        public CtrlGame(DTO.dtoPlayer player) : this()
        {
            _player = player;

            InitiliaseVisuals();  
        }

        public bool GameEnded()
        {
            return false;
        }

        private void InitialiseColumns()
        {
            for (int i = 0; i < _mapSize; i++)
            {
                DataGridViewButtonColumn butCol = new DataGridViewButtonColumn();
                butCol.DefaultCellStyle.SelectionBackColor = Color.LightGreen;
                butCol.DefaultCellStyle.SelectionForeColor = Color.LightGreen;

                dgMap.Columns.Add(butCol);

                DataGridViewButtonColumn enemyCol = new DataGridViewButtonColumn();
                enemyCol.DefaultCellStyle.SelectionBackColor = Color.OrangeRed;
                enemyCol.DefaultCellStyle.SelectionForeColor = Color.OrangeRed;

                dgEnemy.Columns.Add(enemyCol);

            }
        }

        private void InitialiseRows()
        {
            for (int i = 0; i < _mapSize; i++)
            {
                dgMap.Rows.Add();
                dgEnemy.Rows.Add();
            }

            dgMap.CellClick += SelectBtn;
        }

        private void InitiliaseVisuals()
        {
            lblPlayerOne.Text = _player.Name;

            if (_mechanicsController.HasAi)
            {
                lblPlayerTwo.Text = "Bobby Bot";
            }
        }

        #endregion

        #region Selection Phase

        private void SelectBtn(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0) return;

            DataGridViewButtonCell btn = (DataGridViewButtonCell)dgMap.Rows[e.RowIndex].Cells[e.ColumnIndex];
            if (btn.Tag != null && btn.Tag.ToString() == "X")
            {
                UnmarkPosition(btn);
            }
            else
            {
                MarkPosition(btn);
            }
        }

        private void MarkPosition(DataGridViewButtonCell btn)
        {
            if (!_mechanicsController.ShipsLeftToPlace(_playerNo)) return;

            btn.Style.BackColor = Color.DarkBlue;
            btn.Style.ForeColor = Color.DarkBlue;
            btn.Style.SelectionBackColor = Color.DarkBlue;
            btn.Style.SelectionForeColor = Color.DarkBlue;

            _mechanicsController.SelectPoint(btn.ColumnIndex, btn.RowIndex, _playerNo);

            btn.Tag = "X";

        }

        private void UnmarkPosition(DataGridViewButtonCell btn)
        {
            btn.Style.BackColor = Color.WhiteSmoke;
            btn.Style.ForeColor = Color.WhiteSmoke;
            btn.Style.SelectionBackColor = Color.LightGreen;
            btn.Style.SelectionForeColor = Color.LightGreen;

            _mechanicsController.DeselectPoint(btn.ColumnIndex, btn.RowIndex, _playerNo);

            btn.Tag = null;


        }

        #endregion

        #region Attacking/Defending

        private void MarkHit(bool attacked, int posX, int posY)
        {
            DataGridViewButtonCell btn = attacked
                ? (DataGridViewButtonCell)dgMap.Rows[posY].Cells[posX]
                : (DataGridViewButtonCell)dgEnemy.Rows[posY].Cells[posX];

            btn.Style.BackColor = attacked ? Color.Red : Color.Black;
            btn.Style.ForeColor = attacked ? Color.Red : Color.Black;
            btn.Style.SelectionBackColor = attacked ? Color.Red : Color.Black;
            btn.Style.SelectionForeColor = attacked ? Color.Red : Color.Black;
        }

        private void MarkMiss(bool attacked, int posX, int posY)
        {
            DataGridViewButtonCell btn = attacked
                ? (DataGridViewButtonCell)dgMap.Rows[posY].Cells[posX]
                : (DataGridViewButtonCell)dgEnemy.Rows[posY].Cells[posX];

            if (btn.Style.BackColor == Color.Red || btn.Style.BackColor == Color.Black) return;

            btn.Style.BackColor = Color.DarkGoldenrod;
            btn.Style.ForeColor = Color.DarkGoldenrod;
            btn.Style.SelectionBackColor = Color.DarkGoldenrod;
            btn.Style.SelectionForeColor = Color.DarkGoldenrod;

        }

        #endregion

        private GameTurn AttackPosition()
        {            
            DataGridViewButtonCell btn = (DataGridViewButtonCell)dgEnemy.SelectedCells[0];
            
            GameTurn turn = new GameTurn();
            turn.AttackPositionX = btn.ColumnIndex;
            turn.AttackPositionY = btn.RowIndex;
            turn.CurrentPlayer = _playerNo;
            
            return turn;
        }

        #region Events

        private void btnAction_Click(object sender, EventArgs e)
        {
            GameTurn();
        }

        private void dgEnemy_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (_currentState != PlayerState.Attacking) return;
            GameTurn();
        }

        #endregion

        private bool CanAttack()
        {
            if(_mechanicsController.GetCurrentTurn() != _playerNo)
                return false;

            return true;
        }

        private void GameTurn()
        {
            if(_gameOver)
            {
                this.Dispose();
                return;
            }

            if (_currentState == PlayerState.Waiting) return;

            if (_currentState == PlayerState.Selecting)
            {
                if (_mechanicsController.ShipsLeftToPlace(_playerNo)) return;
                btnAction.Text = "Attack";

                dgMap.Enabled = false;
                dgEnemy.Enabled = true;
                dgMap.ClearSelection();

                lblPhase.Text = "Currently Attacking";

                _currentState = PlayerState.Attacking;

                SetCounter();
            }
            else if (_currentState == PlayerState.Attacking)
            {
                GameTurn turn = AttackPosition();

                bool hit = _mechanicsController.AttackPoint(turn);

                if (hit)
                {
                    MarkHit(false, turn.AttackPositionX, turn.AttackPositionY);

                    bool hasWon = _mechanicsController.PlayerHasWon(_playerNo);
                    if(hasWon)
                    {
                        MessageBox.Show("You Won");
                        _gameOver = true;
                    }
                }
                else
                {
                    MarkMiss(false, turn.AttackPositionX, turn.AttackPositionY);
                }

                _currentState = PlayerState.Defending;

                if (_mechanicsController.HasAi)
                {
                    ManageAiTurn();

                    _currentState = PlayerState.Attacking;

                }

                SetCounter();

                if(_gameOver)
                {
                    btnAction.Text = "End Game";
                }

            }

        }

        private void ManageAiTurn()
        {
            GameTurn aiTurn = _mechanicsController.AiTurn();
            bool Aihit = _mechanicsController.AttackPoint(aiTurn);

            if (Aihit)
            {
                MarkHit(true, aiTurn.AttackPositionX, aiTurn.AttackPositionY);
            }
            else
            {
                MarkMiss(true, aiTurn.AttackPositionX, aiTurn.AttackPositionY);
            }

            bool hasWon = _mechanicsController.PlayerHasWon(PlayerNumber.PlayerTwo);
            if (hasWon)
            {
                MessageBox.Show("You Lost");
                _gameOver = true;

            }
        }

        private void SetCounter()
        {
            lblCounter.Text = string.Format("Your Ships left: {0}", _mechanicsController.GetShipsLeft(PlayerNumber.PlayerOne) );

            lblEnemy.Text = string.Format("Enemy Ships left: {0}", _mechanicsController.GetShipsLeft(PlayerNumber.PlayerTwo));

        }

    }
}
