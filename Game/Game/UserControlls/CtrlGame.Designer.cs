﻿namespace Game.UserControlls
{
    partial class CtrlGame
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgMap = new System.Windows.Forms.DataGridView();
            this.btnAction = new System.Windows.Forms.Button();
            this.dgEnemy = new System.Windows.Forms.DataGridView();
            this.pnlBot = new System.Windows.Forms.Panel();
            this.lblEnemy = new System.Windows.Forms.Label();
            this.lblCounter = new System.Windows.Forms.Label();
            this.lblPhase = new System.Windows.Forms.Label();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblPlayerTwo = new System.Windows.Forms.Label();
            this.lblVS = new System.Windows.Forms.Label();
            this.lblPlayerOne = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgEnemy)).BeginInit();
            this.pnlBot.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgMap
            // 
            this.dgMap.AllowUserToAddRows = false;
            this.dgMap.AllowUserToDeleteRows = false;
            this.dgMap.AllowUserToResizeColumns = false;
            this.dgMap.AllowUserToResizeRows = false;
            this.dgMap.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dgMap.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMap.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgMap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMap.ColumnHeadersVisible = false;
            this.dgMap.Location = new System.Drawing.Point(0, 34);
            this.dgMap.MultiSelect = false;
            this.dgMap.Name = "dgMap";
            this.dgMap.RowHeadersVisible = false;
            this.dgMap.Size = new System.Drawing.Size(265, 366);
            this.dgMap.TabIndex = 0;
            // 
            // btnAction
            // 
            this.btnAction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAction.BackColor = System.Drawing.Color.SteelBlue;
            this.btnAction.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnAction.ForeColor = System.Drawing.Color.Orange;
            this.btnAction.Location = new System.Drawing.Point(400, 17);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(126, 43);
            this.btnAction.TabIndex = 1;
            this.btnAction.Text = "-Action-";
            this.btnAction.UseVisualStyleBackColor = false;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // dgEnemy
            // 
            this.dgEnemy.AllowUserToAddRows = false;
            this.dgEnemy.AllowUserToDeleteRows = false;
            this.dgEnemy.AllowUserToResizeColumns = false;
            this.dgEnemy.AllowUserToResizeRows = false;
            this.dgEnemy.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.dgEnemy.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgEnemy.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgEnemy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEnemy.ColumnHeadersVisible = false;
            this.dgEnemy.Location = new System.Drawing.Point(281, 34);
            this.dgEnemy.MultiSelect = false;
            this.dgEnemy.Name = "dgEnemy";
            this.dgEnemy.RowHeadersVisible = false;
            this.dgEnemy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgEnemy.Size = new System.Drawing.Size(265, 366);
            this.dgEnemy.TabIndex = 2;
            this.dgEnemy.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEnemy_CellContentDoubleClick);
            // 
            // pnlBot
            // 
            this.pnlBot.BackColor = System.Drawing.Color.RoyalBlue;
            this.pnlBot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBot.Controls.Add(this.lblEnemy);
            this.pnlBot.Controls.Add(this.lblCounter);
            this.pnlBot.Controls.Add(this.lblPhase);
            this.pnlBot.Controls.Add(this.btnAction);
            this.pnlBot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBot.Location = new System.Drawing.Point(0, 406);
            this.pnlBot.Name = "pnlBot";
            this.pnlBot.Size = new System.Drawing.Size(546, 77);
            this.pnlBot.TabIndex = 3;
            // 
            // lblEnemy
            // 
            this.lblEnemy.AutoSize = true;
            this.lblEnemy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblEnemy.ForeColor = System.Drawing.Color.Orange;
            this.lblEnemy.Location = new System.Drawing.Point(3, 60);
            this.lblEnemy.Name = "lblEnemy";
            this.lblEnemy.Size = new System.Drawing.Size(94, 15);
            this.lblEnemy.TabIndex = 4;
            this.lblEnemy.Text = "Enemy Ships:";
            // 
            // lblCounter
            // 
            this.lblCounter.AutoSize = true;
            this.lblCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblCounter.ForeColor = System.Drawing.Color.Orange;
            this.lblCounter.Location = new System.Drawing.Point(3, 30);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(80, 15);
            this.lblCounter.TabIndex = 3;
            this.lblCounter.Text = "Your Ships:";
            // 
            // lblPhase
            // 
            this.lblPhase.AutoSize = true;
            this.lblPhase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPhase.ForeColor = System.Drawing.Color.Orange;
            this.lblPhase.Location = new System.Drawing.Point(2, 0);
            this.lblPhase.Name = "lblPhase";
            this.lblPhase.Size = new System.Drawing.Size(128, 15);
            this.lblPhase.TabIndex = 2;
            this.lblPhase.Text = "Currently Selecting";
            // 
            // pnlTop
            // 
            this.pnlTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTop.Controls.Add(this.lblPlayerTwo);
            this.pnlTop.Controls.Add(this.lblVS);
            this.pnlTop.Controls.Add(this.lblPlayerOne);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(546, 28);
            this.pnlTop.TabIndex = 4;
            // 
            // lblPlayerTwo
            // 
            this.lblPlayerTwo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPlayerTwo.AutoSize = true;
            this.lblPlayerTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPlayerTwo.ForeColor = System.Drawing.Color.Orange;
            this.lblPlayerTwo.Location = new System.Drawing.Point(449, 0);
            this.lblPlayerTwo.Name = "lblPlayerTwo";
            this.lblPlayerTwo.Size = new System.Drawing.Size(77, 15);
            this.lblPlayerTwo.TabIndex = 5;
            this.lblPlayerTwo.Text = "Player Two";
            this.lblPlayerTwo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblVS
            // 
            this.lblVS.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblVS.AutoSize = true;
            this.lblVS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblVS.ForeColor = System.Drawing.Color.Orange;
            this.lblVS.Location = new System.Drawing.Point(237, 0);
            this.lblVS.Name = "lblVS";
            this.lblVS.Size = new System.Drawing.Size(62, 15);
            this.lblVS.TabIndex = 4;
            this.lblVS.Text = "VERSUS";
            // 
            // lblPlayerOne
            // 
            this.lblPlayerOne.AutoSize = true;
            this.lblPlayerOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPlayerOne.ForeColor = System.Drawing.Color.Orange;
            this.lblPlayerOne.Location = new System.Drawing.Point(3, 0);
            this.lblPlayerOne.Name = "lblPlayerOne";
            this.lblPlayerOne.Size = new System.Drawing.Size(77, 15);
            this.lblPlayerOne.TabIndex = 3;
            this.lblPlayerOne.Text = "Player One";
            // 
            // CtrlGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.pnlBot);
            this.Controls.Add(this.dgEnemy);
            this.Controls.Add(this.dgMap);
            this.Name = "CtrlGame";
            this.Size = new System.Drawing.Size(546, 483);
            ((System.ComponentModel.ISupportInitialize)(this.dgMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgEnemy)).EndInit();
            this.pnlBot.ResumeLayout(false);
            this.pnlBot.PerformLayout();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgMap;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.DataGridView dgEnemy;
        private System.Windows.Forms.Panel pnlBot;
        private System.Windows.Forms.Label lblPhase;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Label lblPlayerTwo;
        private System.Windows.Forms.Label lblVS;
        private System.Windows.Forms.Label lblPlayerOne;
        private System.Windows.Forms.Label lblCounter;
        private System.Windows.Forms.Label lblEnemy;
    }
}
