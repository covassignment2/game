﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Game.UserControlls
{
    public partial class CtrlLeaderboard : UserControl
    {
        private List<DTO.dtoLeadeBoard> _leaderboardData;

        public CtrlLeaderboard()
        {
            InitializeComponent();            
            //ExampleData();
        }

        public CtrlLeaderboard(List<DTO.dtoLeadeBoard> data)
        {
            InitializeComponent();

            _leaderboardData = data;

            PopulateLeaderboard();
        }
                                                                     

        private void PopulateLeaderboard()
        {
            //Return if no entries in collection
            if (!_leaderboardData.Any()) return;
            
            dgLeaderboard.Rows.Clear();

            foreach (DTO.dtoLeadeBoard entry in _leaderboardData)
            {
                dgLeaderboard.Rows.Add(
                    entry.Name,
                    entry.Score,
                    entry.Country);
            }
        }

        public void ClearData()
        {
            if (dgLeaderboard.Rows.Count == 0) return;

            dgLeaderboard.Rows.Clear();
        }      

        //Used for testing purposes, using Model - depracated
        [System.Obsolete]
        private void ExampleData()
        {
           // List<LeaderboardData> exampleData = new List<LeaderboardData>();
           //
           // for (int i = 0; i < 20; i++)
           // {
           //     LeaderboardData ld = new LeaderboardData();
           //     ld.PlayerName = "ExamplePlayer";
           //     ld.Rank = i + 1;
           //     ld.Score = 2000 - i * 100;
           //
           //     ld.Id = 1;
           //     ld.PlayerId = 1;
           //
           //     exampleData.Add(ld);
           // }
           //
           // PopulateLeaderboard(exampleData);
        }

        private void btnBack_Click(object sender, System.EventArgs e)
        {
            this.Dispose();
        }
    }
}
