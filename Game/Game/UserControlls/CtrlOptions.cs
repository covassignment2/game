﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game.UserControlls
{
    public partial class CtrlOptions : UserControl
    {
        public CtrlOptions()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void chckFullScreen_CheckedChanged(object sender, EventArgs e)
        {
            bool isFullscreen = chckFullScreen.Checked;

            //Get MainForm
            Form mainForm = Application.OpenForms[0];

            mainForm.WindowState = isFullscreen 
                ? FormWindowState.Maximized 
                : FormWindowState.Normal;

        }
    }
}
