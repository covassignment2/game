﻿namespace Game.UserControlls
{
    partial class CtrlOptions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlBot = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.lblFullscreen = new System.Windows.Forms.Label();
            this.chckFullScreen = new System.Windows.Forms.CheckBox();
            this.pnlBot.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBot
            // 
            this.pnlBot.BackColor = System.Drawing.Color.RoyalBlue;
            this.pnlBot.Controls.Add(this.btnBack);
            this.pnlBot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBot.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlBot.Location = new System.Drawing.Point(0, 288);
            this.pnlBot.Name = "pnlBot";
            this.pnlBot.Size = new System.Drawing.Size(514, 57);
            this.pnlBot.TabIndex = 2;
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.BackColor = System.Drawing.Color.SteelBlue;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnBack.ForeColor = System.Drawing.Color.Orange;
            this.btnBack.Location = new System.Drawing.Point(415, 3);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(96, 48);
            this.btnBack.TabIndex = 8;
            this.btnBack.Tag = "SP";
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // lblFullscreen
            // 
            this.lblFullscreen.AutoSize = true;
            this.lblFullscreen.Location = new System.Drawing.Point(34, 28);
            this.lblFullscreen.Name = "lblFullscreen";
            this.lblFullscreen.Size = new System.Drawing.Size(57, 13);
            this.lblFullscreen.TabIndex = 3;
            this.lblFullscreen.Text = "FullScreen";
            // 
            // chckFullScreen
            // 
            this.chckFullScreen.AutoSize = true;
            this.chckFullScreen.Location = new System.Drawing.Point(116, 28);
            this.chckFullScreen.Name = "chckFullScreen";
            this.chckFullScreen.Size = new System.Drawing.Size(15, 14);
            this.chckFullScreen.TabIndex = 4;
            this.chckFullScreen.UseVisualStyleBackColor = true;
            this.chckFullScreen.CheckedChanged += new System.EventHandler(this.chckFullScreen_CheckedChanged);
            // 
            // CtrlOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chckFullScreen);
            this.Controls.Add(this.lblFullscreen);
            this.Controls.Add(this.pnlBot);
            this.Name = "CtrlOptions";
            this.Size = new System.Drawing.Size(514, 345);
            this.pnlBot.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlBot;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label lblFullscreen;
        private System.Windows.Forms.CheckBox chckFullScreen;
    }
}
