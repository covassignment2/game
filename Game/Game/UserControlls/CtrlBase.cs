﻿using System.Windows.Forms;

namespace Game.UserControlls
{
    public partial class CtrlBase : UserControl
    {
        public delegate void ChangedEventHandler(object sender, System.EventArgs e);

        public CtrlBase()
        {
            InitializeComponent();
        }

        public bool ControlFinished()
        {
            return true;
        }
    }
}
