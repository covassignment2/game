﻿namespace Game.UserControlls
{
    partial class CtrlLeaderboard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgLeaderboard = new System.Windows.Forms.DataGridView();
            this.pnlBot = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colScore = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colcountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgLeaderboard)).BeginInit();
            this.pnlBot.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgLeaderboard
            // 
            this.dgLeaderboard.AllowUserToAddRows = false;
            this.dgLeaderboard.AllowUserToDeleteRows = false;
            this.dgLeaderboard.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgLeaderboard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLeaderboard.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colName,
            this.colScore,
            this.colcountry});
            this.dgLeaderboard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgLeaderboard.Location = new System.Drawing.Point(0, 0);
            this.dgLeaderboard.Name = "dgLeaderboard";
            this.dgLeaderboard.RowHeadersVisible = false;
            this.dgLeaderboard.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgLeaderboard.Size = new System.Drawing.Size(568, 317);
            this.dgLeaderboard.TabIndex = 0;
            // 
            // pnlBot
            // 
            this.pnlBot.BackColor = System.Drawing.Color.RoyalBlue;
            this.pnlBot.Controls.Add(this.btnBack);
            this.pnlBot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBot.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlBot.Location = new System.Drawing.Point(0, 317);
            this.pnlBot.Name = "pnlBot";
            this.pnlBot.Size = new System.Drawing.Size(568, 57);
            this.pnlBot.TabIndex = 1;
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.BackColor = System.Drawing.Color.SteelBlue;
            this.btnBack.FlatAppearance.BorderSize = 0;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnBack.ForeColor = System.Drawing.Color.Orange;
            this.btnBack.Location = new System.Drawing.Point(469, 6);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(96, 48);
            this.btnBack.TabIndex = 8;
            this.btnBack.Tag = "SP";
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // colName
            // 
            this.colName.HeaderText = "Player Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            // 
            // colScore
            // 
            this.colScore.HeaderText = "Score";
            this.colScore.Name = "colScore";
            this.colScore.ReadOnly = true;
            // 
            // colcountry
            // 
            this.colcountry.HeaderText = "Country";
            this.colcountry.Name = "colcountry";
            this.colcountry.ReadOnly = true;
            // 
            // CtrlLeaderboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgLeaderboard);
            this.Controls.Add(this.pnlBot);
            this.Name = "CtrlLeaderboard";
            this.Size = new System.Drawing.Size(568, 374);
            ((System.ComponentModel.ISupportInitialize)(this.dgLeaderboard)).EndInit();
            this.pnlBot.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgLeaderboard;
        private System.Windows.Forms.Panel pnlBot;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colScore;
        private System.Windows.Forms.DataGridViewTextBoxColumn colcountry;
    }
}
