﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Game
{
    public partial class frmRegister : Form
    {
        private string _password1;
        private string _password2;

        private DTO.dtoPlayer _newPlayerAccount;

        public frmRegister()
        {
            InitializeComponent();

            DisableUnused();
        }

        public DTO.dtoPlayer GetAccount()
        {
            return _newPlayerAccount;
        }

        //Remove data not stored in the db
        private void DisableUnused()
        {
            lblCity.Visible = false;
            lblPass.Visible = false;
            lblPass2.Visible = false;

            txtPass1.Visible = false;
            txtPass2.Visible = false;
            txtCity.Visible = false;

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!ValidateForm()) return;

            _newPlayerAccount = GetAccountFromForm();

            //dbCreate

            string msg = string.Format("Congratulations {0}! A new account has been succesfully created for you!", _newPlayerAccount.Name);


            DialogResult result = MessageBox.Show(msg, "New Account Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = result;           
        }

        private bool ValidateForm()
        {
            bool isValid = true;

            if(txtName.Text == String.Empty)
            {
                isValid = false;
                ValidateControl(txtName, false);
            }                
            else { ValidateControl(txtName, true); }

            //Password no longer required - not stored in the database
            //if (txtPass1.Text == String.Empty ||
            //    txtPass2.Text == String.Empty ||
            //    txtPass1.Text != txtPass2.Text)
            //{
            //    isValid = false;
            //    ValidateControl(txtPass1, false);
            //    ValidateControl(txtPass2, false);
            //
            //}
            //else
            //{
            //    ValidateControl(txtPass1, true);
            //    ValidateControl(txtPass2, true);
            //}

            return isValid;
        }

        private DTO.dtoPlayer GetAccountFromForm()
        {
            DTO.dtoPlayer newPlayerAccount = new DTO.dtoPlayer();

            newPlayerAccount.Name = txtName.Text;
            newPlayerAccount.Country = txtCountry.Text;
           // newPlayerAccount.Password = txtPass1.Text;

            return newPlayerAccount;
        }

        private void txtPass1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPass2_TextChanged(object sender, EventArgs e)
        {

        }

        private void ValidateControl(Control control, bool isValid)
        {
            control.BackColor = isValid ? Color.SteelBlue : Color.DarkRed;
            control.ForeColor = isValid ? Color.Orange : Color.WhiteSmoke;
        }

    }
}
