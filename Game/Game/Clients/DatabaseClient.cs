﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Networking;
using Game.DTO;

namespace Game.Clients
{

    public class DatabaseClient
    {
        private Networking.NetworkConnector _databaseConnector = new Networking.NetworkConnector();

        //public DatabaseClient();

        public DTO.dtoLeaderboardList GetLeaderboardData()
        {
            string leaderboardData = _databaseConnector.GetLeaderboardData();

            DTO.dtoLeaderboardList lbList = new DTO.dtoLeaderboardList();

            lbList.DeserialiseFromJson(leaderboardData);

            return lbList;
        }

        public void AddPlayer(dtoPlayer playerToSerialise)
        {
            string ser = dtoPlayer.SerialiseToJson(playerToSerialise);

            if (string.IsNullOrWhiteSpace(ser)) return;

            _databaseConnector.AddPlayer(ser);
        }
    }
}
