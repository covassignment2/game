﻿using System.Windows.Forms;
using Game.Model;
using Game.Controller;

using Game.Clients;

namespace Game
{
    public partial class MainForm : Form
    {
        private bool _isAuthorised;
        private bool _databaseConnected;
        private bool _serverConnected;

        DTO.dtoPlayer _currentPlayer;

        private NavigationController _navController = new NavigationController();

        private DatabaseClient _dbClient = new DatabaseClient();

        public MainForm()
        {
            InitializeComponent();
            TestServerConnection();
            HideControl(pnlGui);
            ShowControl(pnlLogin);

            GetLeaderboardData();

            //No serever..
            btnStartMulti.Enabled = false;

            //No password stored for authentification
            lblPass.Visible = false;
            txtPass.Visible = false;

            _currentPlayer = new DTO.dtoPlayer();
        }

        void GetLeaderboardData()
        {
            _databaseConnected = true;

            DTO.dtoLeaderboardList dataList = _dbClient.GetLeaderboardData();

            //No data retrieved = no connection
            if(dataList == null || dataList.DataLeaderboard == null)
            {
                _databaseConnected = false;
                return;
            }

            //Pass collection to be used in appropriate control
            _navController.GiveLeaderboardData(dataList.DataLeaderboard);

        }

        void TestServerConnection()
        {
            //No dB, no server, cannot authorise...
            if (!_serverConnected)
            {
                lblFailedLogin.Text = "No Connection to servers";
            };
        }
            
        void AuthoriseLogin()
        {         

            if(txtUser.Text == string.Empty)
            {
                MessageBox.Show("No player Name given.");
                return;
            }

            _currentPlayer.Name = txtUser.Text;
           // _currentPlayer.Password = txtPass.Text;

            //Potential connect to db for authorisation - not implemented
            _isAuthorised = true;

       
            _navController.GivePlayer(_currentPlayer);

        }

        #region Click Events

        //Login click event
        private void authorise_Click(object sender, System.EventArgs e)
        {
            AuthoriseLogin();

            if (_isAuthorised)
            {
                HideControl(pnlLogin);
                ShowControl(pnlGui);
            }
            else
            {
                lblFailedLogin.Visible = true;
            }
        }

        private void btnNewAcc_Click(object sender, System.EventArgs e)
        {
            frmRegister registerForm = new frmRegister();

            DialogResult result = registerForm.ShowDialog();

            if (result != DialogResult.OK) return;

             DTO.dtoPlayer newAccountData = registerForm.GetAccount();

            _dbClient.AddPlayer(newAccountData);

            this._currentPlayer = newAccountData;
            this.txtUser.Text = _currentPlayer.Name;

            //refresh data
            GetLeaderboardData();

            lblPlayer.Text = _currentPlayer.Name;

            return;
        }

        private void HandleNavigation(object sender, System.EventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Tag == null) return;
            string tag = btn.Tag.ToString();

            switch (tag)
            {
                case "SP":
                    _navController.ShowSinglePlayer(pnlControl);
                    break;
                case "MP":
                    break;

                case "LO":
                    if(!_serverConnected)
                    {
                        MessageBox.Show("There is no connection to the Servers!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                    _navController.ShowLobby(pnlControl);
                    break;

                case "LB":
                    _navController.ShowLeaderboard(pnlControl);
                    break;

                case "OP":
                    _navController.ShowOptions(pnlControl);
                    break;

                case "EX":
                    this.Close();
                    break;

                default:
                    break;
            }

        }
        #endregion

        private void HideControl(Control control)
        {
            control.Visible = false;
            control.Dock = DockStyle.None;
            control.SendToBack();
        }

        private void ShowControl(Control control)
        {
            control.Visible = true;
            control.Dock = DockStyle.Fill;
            control.SendToBack();
        }

        private void pnlControl_ControlRemoved(object sender, ControlEventArgs e)
        {
            _navController.UnbindControls((Panel)sender);
        }
    }
}
