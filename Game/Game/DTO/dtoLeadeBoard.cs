﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.DTO;

using Newtonsoft.Json;

namespace Game.DTO
{
    public class dtoLeadeBoard
    {
        public string Name;
        public string Country;
        public float Score;
        public DateTime Time;

    }
}
