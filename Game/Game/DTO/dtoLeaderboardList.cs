﻿using System;
using System.Collections.Generic;

namespace Game.DTO
{
    public class dtoLeaderboardList
    {
        public List<dtoLeadeBoard> DataLeaderboard;

        public string SerialiseToJson()
        {
            return string.Empty;
        }

        public void DeserialiseFromJson(string json)
        {
            try
            {
                dtoLeaderboardList des = (dtoLeaderboardList)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(dtoLeaderboardList));

                if (des.DataLeaderboard == null) return;

                DataLeaderboard = new List<dtoLeadeBoard>();
                foreach (dtoLeadeBoard item in des.DataLeaderboard)
                {
                    DataLeaderboard.Add(item);
                }
            }
            catch (Exception ex)
            {
            }

        }
    }
}
