﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Game.DTO
{
    public class dtoPlayer
    {
        //public int Id;
        public string Country;
        public string Name;
        public int Score;
        //public DateTime date;

        public static string SerialiseToJson(dtoPlayer player)
        {
            try
            {
                string serialised = JsonConvert.SerializeObject(player);
                return serialised;
            }
            catch (Exception ex)
            {
            }

            return string.Empty;
        }
    }
}
