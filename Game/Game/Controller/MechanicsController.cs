﻿using Game.Model;

namespace Game.Controller
{
    class MechanicsController
    {
        public bool HasAi;

        private GameState _currentGameState;
        private const int _noShips = 5;

        public MechanicsController(bool hasAi)
        {
            _currentGameState = new GameState(_noShips, hasAi);

            HasAi = hasAi;
        }

        #region PointSelectionMechanics

        public void SelectPoint(int positionX, int positionY, PlayerNumber player)
        {
            if(player == PlayerNumber.PlayerOne)
            {
                if (!_currentGameState.MapPlayerOne.HasShipsToPlace()) return;
                _currentGameState.MapPlayerOne.SelectPoint(positionX, positionY);
            }
            else
            {
                if (!_currentGameState.MapPlayerTwo.HasShipsToPlace()) return;
                _currentGameState.MapPlayerTwo.SelectPoint(positionX, positionY);
            }
        }

        public void DeselectPoint(int positionX, int positionY, PlayerNumber player)
        {
            if (player == PlayerNumber.PlayerOne)
            {
                _currentGameState.MapPlayerOne.DeselectPoint(positionX, positionY);
            }
            else
            {
                _currentGameState.MapPlayerTwo.DeselectPoint(positionX, positionY);
            }
        }

        public bool AttackPoint(GameTurn turn)
        {
            if(turn.CurrentPlayer == PlayerNumber.PlayerOne)
            {
                _currentGameState.CurrentPlayerTurn = PlayerNumber.PlayerTwo;
                return _currentGameState.MapPlayerTwo.AttackPoint(turn.AttackPositionX, turn.AttackPositionY);
            }
            else
            {
                _currentGameState.CurrentPlayerTurn = PlayerNumber.PlayerOne;
                return _currentGameState.MapPlayerOne.AttackPoint(turn.AttackPositionX, turn.AttackPositionY);
            }
        }

        public GameTurn AiTurn()
        {
            GameTurn aiTurn = new GameTurn();
            System.Random randomPicker = new System.Random();

            bool isValid = false;

            //Ensure AI does not hit the same position twice (alredy hit points)
            while (!isValid)
            {
                aiTurn.AttackPositionX = randomPicker.Next(0, 10);
                aiTurn.AttackPositionY = randomPicker.Next(0, 10);

                if(_currentGameState.MapPlayerOne.EnquirePoint(aiTurn.AttackPositionX, aiTurn.AttackPositionY) != MapComponent.Miss
                 && _currentGameState.MapPlayerOne.EnquirePoint(aiTurn.AttackPositionX, aiTurn.AttackPositionY) != MapComponent.Hit)
                 {
                    isValid = true;
                 }
            }        

            aiTurn.CurrentPlayer = PlayerNumber.PlayerTwo;

            return aiTurn;
        }

        public PlayerNumber GetCurrentTurn()
        {
            return _currentGameState.CurrentPlayerTurn;
        }

        #endregion

        public bool PlayerHasWon(PlayerNumber player)
        {
            if (player == PlayerNumber.PlayerOne)
            {
                bool hasWon = _currentGameState.MapPlayerTwo.HasShipsAlive();
                if(hasWon) { _currentGameState.Winner = PlayerNumber.PlayerOne; }
                return hasWon;
            }
            else
            {
                bool hasWon = _currentGameState.MapPlayerOne.HasShipsAlive();
                if (hasWon) { _currentGameState.Winner = PlayerNumber.PlayerTwo; }
                return hasWon;
            }
        }

        public bool ShipsLeftToPlace(PlayerNumber playerNo)
        {
            return playerNo == PlayerNumber.PlayerOne
                ? _currentGameState.MapPlayerOne.HasShipsToPlace()
                : _currentGameState.MapPlayerOne.HasShipsToPlace();
        }

        public int GetShipsLeft(PlayerNumber playerNo)
        {
            if(playerNo == PlayerNumber.PlayerOne)
            {
                return _currentGameState.MapPlayerOne.ShipsAlive();
            }

            return _currentGameState.MapPlayerTwo.ShipsAlive();

        }

        public GameState GetGameState()
        {
            return _currentGameState;
        }


    }
}
