﻿using System.Windows.Forms;

using Game.UserControlls;
using Game.Model;
using System.Collections.Generic;

namespace Game.Controller
{
    public class NavigationController
    {
        private DTO.dtoPlayer _player;

        public bool _IsDisplaying = false;

        private List<DTO.dtoLeadeBoard> _playerData;

        public NavigationController()
        {
        }

        public void GivePlayer(DTO.dtoPlayer player)
        {
            _player = player;
        }

        public void GiveLeaderboardData(List<DTO.dtoLeadeBoard> leaderboardData)
        {
            _playerData = leaderboardData;
        }

        #region Public

        public void ShowSinglePlayer(Panel panel)
        {
            CtrlGame ctrl = new CtrlGame(_player);
            BindControl(ctrl, panel);                     
        }
        
        public void ShowLeaderboard(Panel panel)
        {
            CtrlLeaderboard ctrl = new CtrlLeaderboard(_playerData);
            BindControl(ctrl, panel);
        }

        public void ShowOptions(Panel panel)
        {
            CtrlOptions ctrl = new CtrlOptions();
            BindControl(ctrl, panel);
        }

        public void ShowLobby(Panel panel)
        {
            CtrlLobby ctrl = new CtrlLobby();
            BindControl(ctrl, panel);
        }

        #endregion

        private void BindControl(Control ctrl, Panel panel)
        {
            panel.Visible = true;

            panel.BringToFront();
            panel.Dock = DockStyle.Fill;
            ctrl.BackColor = System.Drawing.Color.Transparent;

            ctrl.Parent = panel;

            ctrl.Dock = DockStyle.Fill;

            panel.Controls.Add(ctrl);

            _IsDisplaying = true;
        }

        public void UnbindControls(Panel panel)
        {
            panel.Visible = false;

            foreach (Control ctrl in panel.Controls)
            {
                panel.Controls.Remove(ctrl);
            }

            _IsDisplaying = false;

        }
    }
}
