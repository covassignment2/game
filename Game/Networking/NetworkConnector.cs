﻿using System.IO;
using System.Net;
using System.Text;

namespace Networking
{
    public class NetworkConnector
    {
        private const string _URL = "http://301crleaderboard.coventry.domains";
        private const string _DataType = "application/json";
        private const string _POST = "POST";
        private const string _GET = "GET";
        private const string _PASSWORD = "admin";

        private bool _isConnected = false;
        private Stream _dataStream;

        private WebRequest _webRequest;
        
        public bool TestConnection()
        {
            return _isConnected;
        }

        public string GetLeaderboardData()
        {
            string leaderboardJson = RequestGet(SqlCommands.gimmeData);

            return leaderboardJson;
        }

        public void AddPlayer(string jsonData)
        {
            RequestPost(jsonData, SqlCommands.addPlayer);
        }

        #region Private Methods

        private void RequestPost(string jsonData, SqlCommands command)
        {
            string url = CreateUrlCommand(command);
            string paramUrl = string.Format("{0}?input={1}", url, jsonData);

            _webRequest = WebRequest.Create(paramUrl);

            //Password
            string password = _PASSWORD;
            _webRequest.Headers.Add("pass", password);

            byte[] byteArray = Encoding.UTF8.GetBytes(jsonData);
            _webRequest.ContentType = _DataType;
            _webRequest.ContentLength = byteArray.Length;
            _webRequest.Method = _POST;
            using (_dataStream = _webRequest.GetRequestStream())
            {
                _dataStream.Write(byteArray, 0, byteArray.Length);
            }

            HttpWebResponse response = (HttpWebResponse)_webRequest.GetResponse();
            string result;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }

        }

        private string RequestGet(SqlCommands command)
        {
            string url = CreateUrlCommand(command);
            _webRequest = WebRequest.Create(url);
            _webRequest.ContentType = _DataType;
            _webRequest.Method = _GET;

            string responseData = string.Empty;

            using (WebResponse response = _webRequest.GetResponse())
            {
                using (_dataStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(_dataStream))
                    {
                        //May need read line by line
                        responseData = reader.ReadToEnd();
                                                
                    }
                }
            }

            return responseData;
        }

        private string CreateUrlCommand(SqlCommands command)
        {
            string cmd = string.Format("{0}/{1}", _URL, command.ToString());
            return cmd;
        }

        #endregion

    }

    internal enum SqlCommands
    {
        gimmeData = 0, //Get
        addPlayer = 1, //Post
        deleteData = 2 //Post on Id
    }
}
